﻿using System;
namespace RPGCharactersConsoleAppAssignmentOne
{
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException()
        {
        }
        public InvalidArmorException(string message) : base(message)
        {
        }
    }
}
