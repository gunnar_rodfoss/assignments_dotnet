﻿using System;
namespace RPGCharactersConsoleAppAssignmentOne//.Exeptions
{
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException()
        {
        }
        public InvalidWeaponException(string message) : base(message)
        {
     
        }
    }
}
