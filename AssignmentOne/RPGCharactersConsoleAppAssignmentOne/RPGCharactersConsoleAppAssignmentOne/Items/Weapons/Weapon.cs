﻿using System;
namespace RPGCharactersConsoleAppAssignmentOne
{

    public class Weapon 
    {

        public string Name { get; set; }
        public int RequiredLevel { get; set; }
        public string Slot { get; set; }

        public int Damage { get; set; }
        public double AttackSpeed { get; set; }

        public Weapon() {
            Slot = "Weapon";
        }
    }
}
