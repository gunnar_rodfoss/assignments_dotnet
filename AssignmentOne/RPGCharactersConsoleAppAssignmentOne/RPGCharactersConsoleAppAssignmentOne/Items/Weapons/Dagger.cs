﻿using System;
namespace RPGCharactersConsoleAppAssignmentOne.Items.Wepons
{
    public class Dagger : Weapon
    {
        public Dagger()
        {
            Name = "Dagger";
            RequiredLevel = 2;
            AttackSpeed = 7.4;
            Damage = 1;
        }
    }
}
