﻿using System;
namespace RPGCharactersConsoleAppAssignmentOne.Items.Wepons
{
    public class Hammer : Weapon
    {
        public Hammer()
        {
            Name = "Hammer";
            RequiredLevel = 2;
            AttackSpeed = 3.4;
            Damage = 1;
        }
    }
}
