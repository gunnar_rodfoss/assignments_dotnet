﻿using System;
namespace RPGCharactersConsoleAppAssignmentOne
{
    public class Axe : Weapon
    {
        public Axe()
        {
            Name = "Axe";
            RequiredLevel = 3;
            AttackSpeed = 1.1;
            Damage = 7;
        }
    }
}
