﻿using System;
namespace RPGCharactersConsoleAppAssignmentOne
{
    public class Wand : Weapon
    {
        public Wand()
        {
            Name = "Wand";
            RequiredLevel = 10;
            AttackSpeed = 3.4;
            Damage = 1;
        }
    }
}
