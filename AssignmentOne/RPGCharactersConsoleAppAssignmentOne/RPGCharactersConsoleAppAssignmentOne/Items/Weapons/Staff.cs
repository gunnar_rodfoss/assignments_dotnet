﻿using System;
namespace RPGCharactersConsoleAppAssignmentOne
{
    public class Staff : Weapon
    {

        public Staff()
        {
            Name = "Staff";
            RequiredLevel = 1;
            Damage = 7;
            AttackSpeed = 1.1;
        }
    }
}
