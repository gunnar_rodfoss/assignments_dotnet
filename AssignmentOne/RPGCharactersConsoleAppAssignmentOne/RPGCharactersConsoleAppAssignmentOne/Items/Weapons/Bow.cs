﻿using System;
namespace RPGCharactersConsoleAppAssignmentOne
{
    public class Bow : Weapon
    {
        public Bow()
        {
            Name = "Bow";
            RequiredLevel = 6;
            AttackSpeed = 3.4;
            Damage = 1;
        }
    }
}
