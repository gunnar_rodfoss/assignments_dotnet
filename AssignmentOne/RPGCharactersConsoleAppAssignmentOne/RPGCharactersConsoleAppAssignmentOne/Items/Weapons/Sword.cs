﻿using System;
namespace RPGCharactersConsoleAppAssignmentOne
{
    public class Sword : Weapon
    {
        public Sword()
        {
            Name = "Sword";
            RequiredLevel = 10;
            AttackSpeed = 3.4;
            Damage = 3;
        }
    }
}
