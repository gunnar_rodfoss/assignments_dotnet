﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharactersConsoleAppAssignmentOne
{
    public class Armor 
    {
        public string Name { get; set; }
        public int RequiredLevel { get; set; }
        public string Slot { get; set; }
    }
}
