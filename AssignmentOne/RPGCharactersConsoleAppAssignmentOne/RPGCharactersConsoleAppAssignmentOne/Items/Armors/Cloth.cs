﻿using System;
namespace RPGCharactersConsoleAppAssignmentOne
{
    public class Cloth : Armor
    {
        public Cloth()
        {
            Name = "Cloth";
            RequiredLevel = 1;
            Slot = "Body";
        }
    }
}
