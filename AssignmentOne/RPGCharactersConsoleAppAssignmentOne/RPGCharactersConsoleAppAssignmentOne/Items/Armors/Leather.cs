﻿using System;
namespace RPGCharactersConsoleAppAssignmentOne
{
    public class Leather : Armor
    {
        public Leather()
        {
            Name = "Leather";
            RequiredLevel = 4;
            Slot = "Legs";
        }
    }
}
