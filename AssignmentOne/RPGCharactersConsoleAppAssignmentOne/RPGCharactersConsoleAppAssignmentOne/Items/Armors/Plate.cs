﻿using System;
namespace RPGCharactersConsoleAppAssignmentOne
{
    public class Plate : Armor
    {
        public Plate()
        {
            Name = "Plate";
            RequiredLevel = 10;
            Slot = "Body";
        }
    }
}
