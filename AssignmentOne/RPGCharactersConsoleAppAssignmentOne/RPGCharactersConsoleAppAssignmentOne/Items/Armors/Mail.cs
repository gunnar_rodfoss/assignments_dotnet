﻿using System;
namespace RPGCharactersConsoleAppAssignmentOne
{
    public class Mail : Armor
    {
        public Mail()
        {
            Name = "Mail";
            RequiredLevel = 10;
            Slot = "Head";
        }
    }
}
