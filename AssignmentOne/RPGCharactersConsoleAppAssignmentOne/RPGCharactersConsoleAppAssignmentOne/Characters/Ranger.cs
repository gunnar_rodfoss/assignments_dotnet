﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharactersConsoleAppAssignmentOne
{
    public class Ranger: Character
    {
        /// <summary>
        /// Ranger Constructor Name defaults to Ranger or pass new Name as string parameter
        /// </summary>
        public Ranger()
        {
            Name = this.GetType().Name;
            Dexterity = 7;
            WeaponType = new string[] { "Bow" };
            ArmorType = new string[] { "Mail", "Leather" };
            Damage = Dexterity;
        }
        /// <summary>
        /// Ranger Constructor pass new Name as string parameter
        /// </summary>
        public Ranger(string Name)
        {
            
            this.Name = Name;
            Dexterity = 7;
            WeaponType = new string[] { "Bow" };
            ArmorType = new string[] { "Mail", "Leather" };
            Damage = Dexterity;
        }
        public override void IncreasLevel()
        {
            Level++;
            Strength++;
            Dexterity+=5;
            Intelligence++;
            Damage = Dexterity;
        }
    }
}
