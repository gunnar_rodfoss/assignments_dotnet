﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharactersConsoleAppAssignmentOne
{
    public class Warrior: Character
    {
        /// <summary>
        /// Warrior Constructor Name defaults to Warrior or pass new Name as string parameter
        /// </summary>
        public Warrior()
        {
            Name = this.GetType().Name;
            Strength = 5;
            Dexterity = 2;
            WeaponType = new string[] { "Axe", "Hammer", "Sword" };
            ArmorType = new string[] { "Mail", "Plate" };
            Damage = Strength;
        }
        /// <summary>
        /// Warrior Constructor pass new Name as string parameter
        /// </summary>
        public Warrior(string Name)
        {
            this.Name = Name;
            Strength = 5;
            Dexterity = 2;
            WeaponType = new string[]{ "Axe", "Hammer", "Sword" };
            ArmorType = new string[] { "Mail", "Plate" };
            Damage = Strength; 
        }
        public override void IncreasLevel() 
        {
            Strength += 3;
            Dexterity += 2;
            Intelligence++;
            Damage = Strength;
            Level++;
        }
    }
}
