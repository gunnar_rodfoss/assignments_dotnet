﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharactersConsoleAppAssignmentOne
{
    public class Mage : Character
    {
        /// <summary>
        /// Mage Constructor Name defaults to Mage or pass new Name as string parameter
        /// </summary>
        public Mage()
        {
            Name = this.GetType().Name;
            Intelligence = 8;
            WeaponType = new string[] { "Staff", "Wand" };
            ArmorType = new string[] { "Cloth" };
            Damage = this.Intelligence;
        }
        /// <summary>
        /// Mage Constructor pass new Name as string parameter
        /// </summary>
        public Mage(string Name) 
        { 
            this.Name = Name;
            Intelligence = 8;
            WeaponType = new string[] { "Staff", "Wand" };
            ArmorType = new string[] { "Cloth" };
            Damage = this.Intelligence;
        }

        

        public override void IncreasLevel()
        {
            Level++;
            Strength++;
            Dexterity++;
            Intelligence+=5;
            Damage=Intelligence;
        }
    }
}
