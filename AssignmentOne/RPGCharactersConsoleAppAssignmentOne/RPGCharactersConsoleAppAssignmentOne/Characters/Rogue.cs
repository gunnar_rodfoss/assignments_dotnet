﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharactersConsoleAppAssignmentOne
{
    public class Rogue: Character
    {
        /// <summary>
        /// Rogue Constructor Name defaults to Rogue or pass new Name as string parameter
        /// </summary>
        public Rogue() 
        { 
            Name=this.GetType().Name;
            Strength = 2;
            Dexterity = 6;
            WeaponType = new string[] { "Dagger", "Sword" };
            ArmorType = new string[] { "Mail", "Leather" };
            Damage = Dexterity;
        }
        /// <summary>
        /// Rogue Constructor pass new Name as string parameter
        /// </summary>
        public Rogue(string Name)
        {
            this.Name = Name;
            Strength = 2;
            Dexterity = 6;
            WeaponType = new string[] { "Dagger", "Sword" };
            ArmorType = new string[] { "Mail", "Leather" };
            Damage = Dexterity;
        }
        public override void IncreasLevel()
        {
            Level++;
            Strength+=1;
            Dexterity+=4;
            Intelligence++;
            Damage = Dexterity;
        }
    }
}
