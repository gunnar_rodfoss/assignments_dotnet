﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharactersConsoleAppAssignmentOne
{
    public abstract class Character
    {
        public string Name { get; set; } 
        public int Level { get; set; } = 1;
        public int Strength { get; set; } = 1;
        public int Dexterity { get; set; } = 1;
        public int Intelligence { get; set; } = 1;
        public int Damage { get; set; }


        public string[] WeaponType { get; set; }
        
        public string[] ArmorType { get; set; }

        private Dictionary<string, Armor> ArmorDictionary = new Dictionary<string, Armor> { { "Head", null }, { "Body", null }, { "Legs", null } };

        private Dictionary<string, Weapon> WeaponDictionary = new Dictionary<string, Weapon> { { "Weapon", null } };

        /// <summary>
        /// Increases the Attributs of the Character
        /// </summary>
        public abstract void IncreasLevel();
        /// <summary>
        /// Returns a stats message string of the attributs Name, Strength, Dexterity, Intelligence, Damage
        /// </summary>
        /// <returns></returns>
        public string Stats()
        {
            return $"Name: {Name}, Strength: {Strength}, Dexterity: {Dexterity}, Intelligence: {Intelligence}, Damage: {Damage}";
        }
        /// <summary>
        /// Takes a Weapon object as a parameter
        /// -Axes
        /// -Bows
        /// -Daggers
        /// -Hammers
        /// -Staffs
        /// -Swords
        /// -Wands
        /// </summary>
        /// <param name="Weapon"></param>
        /// <returns>Returns a string message on the equipt weapon type</returns>
        public string EquiptWeapon(Weapon weapon)
        {

            if (WeaponDictionary.ContainsKey(weapon.Slot) && 
                WeaponType.Contains(weapon.Name) && 
                this.Level >= (weapon.RequiredLevel))
            {
            WeaponDictionary[weapon.Slot] = weapon; 
            return $"New {weapon.Name} weapon equipped!";
            }
            else
                throw new InvalidWeaponException($"Level is to low or character can't use weapon of type {weapon.Name}");
        }
        /// <summary>
        /// Takes a Armor object as a parameter and
        /// -Cloth
        /// -Leather
        /// -Mail
        /// -Plate
        /// </summary>
        /// <param name="Armor"></param>
        /// <returns>Returns a string message on the equipt armor type</returns>
        public string EquiptArmor(Armor armor)
        {
            if (ArmorDictionary.ContainsKey(armor.Slot) && 
                ArmorType.Contains(armor.Name) && 
                this.Level >= (armor.RequiredLevel))
            {
                ArmorDictionary[armor.Slot]= armor;
                return $"New {armor.Name} armour equipped!";
            }
                    
            else
                throw new InvalidArmorException($"Level is to low or character can't use Armor of type {armor.Name}");
        }
        //Calculates the damage given by the equipted armor gives one point for every Armor equipt
        private double ArmorDamage()
        {
            double sum = 0.0;
            if (ArmorDictionary["Head"] != null) { sum++; }
            if (ArmorDictionary["Body"] != null) { sum++; }
            if (ArmorDictionary["Legs"] != null) { sum++; }
            return sum;
        }
        //Calculates the damage given by the equipted weapon
        private double WeaponDamage()
        {
            double sum = 1.0;
            if (WeaponDictionary["Weapon"] != null) { sum = WeaponDictionary["Weapon"].Damage * (double)WeaponDictionary["Weapon"].AttackSpeed; }
            return sum;
        }
        /// <summary>
        /// Calculates the damage given by a character and all it's armor and weapon equipped
        /// </summary>
        /// <returns>Returns a double value of the Characters damage</returns>
        public double GetDamage()
        {
            return Double.Parse((( WeaponDamage() * (1 + (  ArmorDamage()+Damage  ) / 100 ) ).ToString("0.000")));
        }

    }
}
