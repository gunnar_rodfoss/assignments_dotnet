﻿using RPGCharactersConsoleAppAssignmentOne;
using System;
using Xunit;


namespace RPGCharactersConsoleAppAssignmentOne_Tests
{
    public class WarriorTests
    {
        [Fact]
        public void Constructor_SeeIfTheNameOfTheWarriorEqualsClassName_ShuldBeWarrior()
        {
            //Arrange
            string Name = "Warrior";
            Warrior warrior = new Warrior();
            //Act
            string actName = warrior.Name;
            //Assert
            Assert.Equal(actName, Name);
        }
        [Fact]
        public void Constructor_SeeIfWeCanSetTheNameofTheCharacter_ShuldBeGunnar()
        {
            //Arrange
            string Name = "Gunnar";
            Warrior warrior = new Warrior(Name);
            //Act
            string actName = warrior.Name;
            //Assert
            Assert.Equal(actName, Name);
        }
        [Fact]
        public void InvalidWeaponException_TriesToEquipAHighLevelWeaponType_shouldReturnAInvalidWeaponException()
        {
            //Arrange
            Warrior warrior = new Warrior();
            //Assert Act 
            Assert.Throws<InvalidWeaponException>(() => { warrior.EquiptWeapon(new Bow() { RequiredLevel = 9}); });
        }
        [Fact]
        public void InvalidArrmorException_TriesToEquipAToHighLevelArrmor_ShouldReturnInvalidArrmorException()
        {
            //Arrange
            Warrior warrior = new Warrior();
            //Assert Act 
            Assert.Throws<InvalidArmorException>(() => { warrior.EquiptArmor(new Leather() { RequiredLevel = 12 }); });
        }
        [Fact]
        public void InvalidWeaponException_TriesToEquipTheWrongWeaponType_shouldReturnAInvalidWeaponException()
        {
            //Arrange
            Warrior warrior = new Warrior();
            //Assert Act 
            Assert.Throws<InvalidWeaponException>(() => { warrior.EquiptWeapon(new Bow()); });
        }
        [Fact]
        public void InvalidArrmorException_TriesToEquipTheWrongArrmorType_ShouldReturnInvalidArrmorException()
        {
            //Arrange
            Warrior warrior = new Warrior();
            //Assert Act 
            Assert.Throws<InvalidArmorException>(() => { warrior.EquiptArmor(new Plate()); });
        }
        [Fact]
        public void EquiptWeapon_WeponEquiptedSeuccessMessage_ShouldReturnNewSwordWeaponEquipped()
        {
            //Arrange
            string Message = "New Sword weapon equipped!";
            Warrior warrior = new Warrior() { Level = 10 };
            //Act
            string actMessage = warrior.EquiptWeapon(new Sword());
            //Assert 
            Assert.Equal(actMessage, Message);
        }
        [Fact]
        public void EquiptArmor_ArmorEquiptedSeuccessMessage_ShouldReturnNewMailArmourEquipped()
        {
            //Arrange
            string Message = "New Mail armour equipped!";
            Warrior warrior = new Warrior() { Level = 100 };
            //Act
            string actMessage = warrior.EquiptArmor(new Mail());
            //Assert 
            Assert.Equal(actMessage, Message);
        }



        





        [Fact]
        public void Calculate_ExpectedDamegeNoWeaponNoArmorEquipped_ShouldReturnDoubleValueNumber()
        {
            //Arrange
            double Damage = 1.05;
            Warrior warrior = new Warrior();
            //Act
            double actDamage = warrior.GetDamage();
            //Assert 
            Assert.Equal(Damage, actDamage);
        }
        [Fact]
        public void Calculate_ExpectedDamegeWeaponNoArmorEquipped_ShouldReturnDoubleValueNumber()
        {
            //Arrange
            double Damage = 8.085;
            Warrior warrior = new Warrior() { Level=3};
            warrior.EquiptWeapon(new Axe());
            //Act
            double actDamage = warrior.GetDamage();
            //Assert 
            Assert.Equal(Damage, actDamage);
        }
        [Fact]
        public void Calculate_ExpectedDamegeWeaponAndArmorEquipped_ShouldReturnDoubleValueNumber()
        {
            //Arrange
            double Damage = 8.162;
            Warrior warrior = new Warrior() { Level = 10 };
            warrior.EquiptWeapon(new Axe());
            warrior.EquiptArmor(new Plate());
            //Act
            double actDamage = warrior.GetDamage();
            //Assert 
            Assert.Equal(Damage, actDamage);
        }
        [Fact]
        public void NewWeapon_WeponEquippedTestToSeeIfWeaponGetsAddedToWeaponSlot_ShouldReturnSword()
        {
            //Arrange
            string weapon = "Sword";
            Warrior warrior = new Warrior() { Level = 10 };
            warrior.EquiptWeapon(new Sword());
            //Act
            string actWeapon = warrior.WeaponType[2];
            //Assert
            Assert.Equal(actWeapon, weapon);
        }
        [Fact]
        public void Stats_CharacterStatsDisplayToString_ShouldReturnStatsString()
        {
            //Arrange
            string message = "Name: Warrior, Strength: 5, Dexterity: 2, Intelligence: 1, Damage: 5";
            Warrior warrior = new Warrior();
            //Act
            string actMessage = warrior.Stats();
            //Assert 
            Assert.Equal(message, actMessage);
        }
    }
}
