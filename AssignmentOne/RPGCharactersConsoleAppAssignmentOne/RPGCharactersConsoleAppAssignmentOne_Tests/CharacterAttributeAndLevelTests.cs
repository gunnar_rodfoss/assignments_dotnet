﻿using RPGCharactersConsoleAppAssignmentOne;
using System;
using Xunit;

namespace RPGCharactersConsoleAppAssignmentOne_Tests
{
    public class CharacterAttributeAndLevelTests
    {
        [Fact]
        public void Constructor_SeeIfWeGetExpectedLevelValue_ShouldBeOne()
        {
            //Arrange
            int Level = 1;
            Warrior warrior = new Warrior();
            //Act
            int actLevel = warrior.Level;
            //Assert
            Assert.Equal(Level, actLevel);
        }
        [Fact]
        public void IncreasLevel_SeeIfCharacterGainsALevelAfterInvokingTheIncreasLevelMethod_ShuldBeTwo()
        {
            //Arrange
            int Level = 2;
            Warrior warrior = new Warrior();
            warrior.IncreasLevel();
            //Act
            int actLevel = warrior.Level;
            //Assert
            Assert.Equal(Level, actLevel);
        }
        [Fact]
        public void Strength_SeeIfWeGetDefaultValueOnStrengt_ShuldBeFive()
        {
            //Arrange
            int Strength = 5;
            Warrior warrior = new Warrior();
            //Act
            int actStrength = warrior.Strength;
            //Assert
            Assert.Equal(Strength, actStrength);
        }
        [Fact]
        public void Dexterity_SeeIfWeGetDefaultValueOnDexterity_ShuldBeTwo()
        {
            //Arrange
            int Dexterity = 2;
            Warrior warrior = new Warrior();
            //Act
            int actDexterity = warrior.Dexterity;
            //Assert
            Assert.Equal(Dexterity, actDexterity);
        }
        [Fact]
        public void Intelligence_SeeIfWeGetDefaultValueOnIntelligence_ShuldBeOne()
        {
            //Arrange
            int Intelligence = 1;
            Warrior warrior = new Warrior();
            //Act
            int actIntelligence = warrior.Intelligence;
            //Assert
            Assert.Equal(Intelligence, actIntelligence);
        }
        [Fact]
        public void Damage_SeeIfWeGetDefaultValueOnDamage_ShuldBeFive()
        {
            //Arrange
            int Damage = 5;
            Warrior warrior = new Warrior();
            //Act
            int actDamage = warrior.Damage;
            //Assert
            Assert.Equal(Damage, actDamage);
        }
        [Fact]
        public void IncreasLevel_SeeIfWeGetValueOnStrengtAfterInvokingTheIncreaseLevelMethod_ShuldBeEight()
        {
            //Arrange
            int Strength = 8;
            Warrior warrior = new Warrior();
            warrior.IncreasLevel();
            //Act
            int actStrength = warrior.Strength;
            //Assert
            Assert.Equal(Strength, actStrength);
        }
        [Fact]
        public void IncreasLevel_SeeIfWeGetValueOnDexterityAfterInvokingTheIncreaseLevelMethod_ShuldBeFour()
        {
            //Arrange
            int Dexterity = 4;
            Warrior warrior = new Warrior();
            warrior.IncreasLevel();
            //Act
            int actDexterity = warrior.Dexterity;
            //Assert
            Assert.Equal(Dexterity, actDexterity);
        }
        [Fact]
        public void IncreasLevel_SeeIfWeGetValueOnIntelligenceAfterInvokingTheIncreaseLevelMethod_levelingUp()
        {
            //Arrange
            int Intelligence = 2;
            Warrior warrior = new Warrior();
            warrior.IncreasLevel();
            //Act
            int actIntelligence = warrior.Intelligence;
            //Assert
            Assert.Equal(Intelligence, actIntelligence);
        }
        [Fact]
        public void IncreasLevel_SeeIfWeGetValueOnDamageAfterInvokingTheIncreaseLevelMethod_ShuldBeEight()
        {
            //Arrange
            int Damage = 8;
            Warrior warrior = new Warrior();
            warrior.IncreasLevel();
            //Act
            int actDamage = warrior.Damage;
            //Assert
            Assert.Equal(Damage, actDamage);
        }
    }
}
